﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public float smoothing = 5f;
    public GameObject camera;
    Vector3 offset;
    GameManager gameManager;
    private float jumpIter = 9.5f;
    public AnimationCurve curve;
    public float shakeDuration = 1f;

    void Start() {
        gameManager = GameObject.Find("GameController").GetComponent<GameManager>();
    }

    void OnEnable(){
	    EventsManager.SetCameraTargetEvent += SetCameraTargetEvent;
        EventsManager.ShakeCameraEvent += ShakeCameraEvent;
        EventsManager.CleanCameraTargetEvent += CleanCameraTargetEvent;
        	EventsManager.ShakeViewEvent += ShakeViewEvent;
    }
    void OnDisable(){
	    EventsManager.SetCameraTargetEvent -= SetCameraTargetEvent;
        EventsManager.ShakeCameraEvent -= ShakeCameraEvent;
        EventsManager.CleanCameraTargetEvent -= CleanCameraTargetEvent;
        	EventsManager.ShakeViewEvent -= ShakeViewEvent;
    }

    void SetCameraTargetEvent(GameObject targetObject){
        LeanTween.followDamp(camera.gameObject.transform, targetObject.transform, LeanProp.localPosition, 0.3f);
    }

    void ShakeViewEvent(){
        Shaking();
    }

    IEnumerator Shaking() { 
        Debug.Log("SHAKING!");
        Vector3 startPosition = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < shakeDuration) {
            elapsedTime += Time.deltaTime;
            float strength = curve.Evaluate(elapsedTime / shakeDuration);
            camera.transform.position = startPosition + Random.insideUnitSphere * strength;
            yield return null;
        }

        camera.transform.position = startPosition;
    }

    void ShakeCameraEvent(float dropOffTime){
        float height = Mathf.PerlinNoise(jumpIter, 0f)*10f;
		height = height*height * 5f;

        LeanTween.cancel(gameObject);

        /**************
        * Camera Shake
        **************/
        
        float shakeAmt = height*0.2f; // the degrees to shake the camera
        float shakePeriodTime = 0.3f; // The period of each shake
        LTDescr shakeTween = LeanTween.rotateAroundLocal( gameObject, Vector3.right, shakeAmt, shakePeriodTime)
        .setEase( LeanTweenType.easeShake ) // this is a special ease that is good for shaking
        .setLoopClamp()
        .setRepeat(-1);

        // Slow the camera shake down to zero
        LeanTween.value(gameObject, shakeAmt, 0f, dropOffTime).setOnUpdate( 
            (float val)=>{
                shakeTween.setTo(Vector3.right*val);
            }
        ).setEase(LeanTweenType.easeOutQuad);
    }

    void CleanCameraTargetEvent(){
        Debug.Log("TARGET CLEAN");
    }
}