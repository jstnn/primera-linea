using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager : MonoBehaviour
{
    public delegate void ShakeCameraDelegate(float duration);
    public static event ShakeCameraDelegate ShakeCameraEvent;
    public static void ShakeCamera(float duration){
        if(ShakeCameraEvent != null)
            ShakeCameraEvent(duration);
    }

    public delegate void SetCameraTargetDelegate(GameObject targetObject);
    public static event SetCameraTargetDelegate SetCameraTargetEvent;
    public static void SetCameraTarget(GameObject targetObject){
        if(SetCameraTargetEvent != null)
            SetCameraTargetEvent(targetObject);
    }

    public delegate void CleanCameraTargetDelegate();
    public static event CleanCameraTargetDelegate CleanCameraTargetEvent;
    public static void CleanCameraTarget(){
        if(CleanCameraTargetEvent != null)
            CleanCameraTargetEvent();
    }

    public delegate void AddToInventoryDelegate(QuantumItem item);
    public static event AddToInventoryDelegate AddToInventoryEvent;
    public static void AddToInventory(QuantumItem item){
        if(AddToInventoryEvent != null)
            AddToInventoryEvent(item);
    }

    public delegate void RemovePlayerDelegate(GameObject player);
    public static event RemovePlayerDelegate RemovePlayerEvent;
    public static void RemovePlayer(GameObject player){
        if(RemovePlayerEvent != null)
            RemovePlayerEvent(player);
    }

    public delegate void ShakeViewDelegate();
public static event ShakeViewDelegate ShakeViewEvent;
public static void ShakeView(){
	if(ShakeViewEvent != null)
		ShakeViewEvent();
}
}
