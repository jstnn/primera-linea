﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{

    public Transform[] Navs;
    NavMeshAgent agent;
    float onMeshThreshold = 3;

public bool IsAgentOnNavMesh(GameObject agentObject)
{
    Vector3 agentPosition = agentObject.transform.position;
    NavMeshHit hit;

    // Check for nearest point on navmesh to agent, within onMeshThreshold
    if (NavMesh.SamplePosition(agentPosition, out hit, onMeshThreshold, NavMesh.AllAreas))
    {
        // Check if the positions are vertically aligned
        if (Mathf.Approximately(agentPosition.x, hit.position.x)
            && Mathf.Approximately(agentPosition.z, hit.position.z))
        {
            // Lastly, check if object is below navmesh
            return agentPosition.y >= hit.position.y;
        }
    }

    return false;
}

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (!IsAgentOnNavMesh(agent.gameObject))
            return;

        if (Navs != null && agent.velocity.magnitude == 0f)
        {
            agent.SetDestination(Navs[Random.Range(0, Navs.Length)].position);
        }
    }
}
