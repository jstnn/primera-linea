using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadItemsIntoContainer : MonoBehaviour
{
    [SerializeField] List<GameObject> prefabPool = new List<GameObject>();
    [SerializeField] int numberOfItems = 10;

    public QuantumContainer container;

    void Start()
    {
        container = GetComponent<QuantumContainer>();
        prefabCreation();
    }

    public void prefabCreation()
    {
        GameObject[] itemPrefabs = Resources.LoadAll<GameObject>("Items");
        int itemsLength = itemPrefabs.Length;

        if (itemsLength != 0)
        {
            foreach (GameObject prefab in itemPrefabs)
            {
                prefabPool.Add(prefab);
            }
            for (int i = 0; i < numberOfItems; i++)
            {
                int randomNumber = Random.Range(0, itemsLength);
                QuantumItem item = prefabPool[randomNumber].GetComponent<QuantumItem>();
                container.inventory.Add(new QuantumInventory.Slot(item.item, item.type, item.quantity, item.icon, item.stackable, item.metaData));
            }
        }
    }
}