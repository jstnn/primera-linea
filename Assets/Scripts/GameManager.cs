﻿using UnityEngine;
using System.Collections;
using MonsterLove.StateMachine;
using System;
using System.Collections.Generic;
using System.Linq;


public class GameManager : MonoBehaviour
{
    public enum States
    {
        Init,
        Intro,
        Countdown,
        Play,
        Win,
        Lose
    }
    public int startingPlayers = 1;
    public Transform startPoint;
    public GameObject prefab;
    public List<Player> players = new List<Player>();
    public CameraController camera;
    public GameObject camera1;
    public GameObject camera2;
    public GameObject camera3;

    public GameObject[] itemPrefabs;
    private StateMachine<States> fsm;
    private SemihOrhan.WaveOne.Demo.DisplayStats displayStats;
    private bool endedGame = false;

    private void Awake()
    {
        fsm = StateMachine<States>.Initialize(this, States.Init);
        camera1.SetActive(true);
        camera2.SetActive(false);
        camera3.SetActive(false);
        itemPrefabs = Resources.LoadAll<GameObject>("Items");
    }

    void Update() {
        if (!displayStats)
            displayStats = GameObject.Find("Canvas").GetComponent<SemihOrhan.WaveOne.Demo.DisplayStats>();
    }

    public void StartGame() {
        Debug.Log("PRESSED START");
        fsm.ChangeState(States.Intro);
    }

    public void WinGame() {
        Debug.Log("PRESSED START");
        fsm.ChangeState(States.Win);
    }

    void OnEnable(){
	EventsManager.RemovePlayerEvent += RemovePlayerEvent;
}
    void OnDisable(){
        EventsManager.RemovePlayerEvent -= RemovePlayerEvent;
    }
    void RemovePlayerEvent(GameObject player){
        players.Remove(player.GetComponent<Player>());
        Debug.Log(player);
    }

    public void AddPlayer() {
        GameObject rioter = Instantiate(prefab, startPoint.position, Quaternion.identity) as GameObject;
        players.Add(rioter.GetComponent<Player>());
    }

    private bool isEverybodyDead() {
     for (int i = 0; i < players.Count; ++i) {
        if (players[i].isDead == false) {
            return false;
        }
    }
        endedGame=true;
        return true;
    }

    private void Init_Enter()
    {
        endedGame=false;
        Debug.Log("START GAME");
        Debug.Log("Waiting for start button to be pressed");
        if (displayStats)
            displayStats.SetCanvas("Init");
    }

    private void Init_Update()
    {
        if (!displayStats) {
            displayStats = GameObject.Find("Canvas").GetComponent<SemihOrhan.WaveOne.Demo.DisplayStats>();
            displayStats.SetCanvas("Init");
        }
    }
    private IEnumerator Intro_Enter() {
        for (int i = 0; i < players.Count; i++)
            Destroy(players[i].gameObject);

        for (int i = 0; i < startingPlayers; i++)
            AddPlayer();

        displayStats.SetCanvas("Countdown");
        displayStats.SetCountdown("Debes encontrar a tu hija en la multitud");
        yield return new WaitForSeconds(1f);
        fsm.ChangeState(States.Countdown);
    }

    private void Intro_Exit() {
        camera1.SetActive(false);
        camera2.SetActive(true);
        camera3.SetActive(false);
    }

    private IEnumerator Countdown_Enter()
    {  
        displayStats.SetCanvas("Countdown");
        displayStats.SetCountdown("Starting in 3...");
        yield return new WaitForSeconds(0.5f);
        displayStats.SetCountdown("Starting in 2...");
        yield return new WaitForSeconds(0.5f);
        displayStats.SetCountdown("Starting in 1...");
        yield return new WaitForSeconds(0.5f);

        fsm.ChangeState(States.Play);
    }

    private void Play_Enter()
    {
        endedGame=false;
        displayStats.SetCanvas("Play");
        Debug.Log("FIGHT!");
        
    }

    private void Play_Update()
    {
        if (endedGame) return;
        if (!endedGame && isEverybodyDead())
            fsm.ChangeState(States.Lose);

        if (players.Count > 0) {
            displayStats.SetHealth((int)players.First().health);
            displayStats.SetAlivePlayers(players.Count);
        }
    }

    void Play_Exit()
    {
        EventsManager.CleanCameraTarget();
        camera1.SetActive(false);
        camera2.SetActive(false);
        camera3.SetActive(true);
    }

    void Lose_Enter()
    {
        endedGame=true;
        displayStats.SetCanvas("Lose");
    }

    void Win_Enter()
    {
        endedGame=true;
        displayStats.SetCanvas("Win");
    }

    // void OnGUI()
    // {
    //     //Example of polling state 
    //     var state = fsm.State;

    //     GUILayout.BeginArea(new Rect(50, 50, 120, 40));

    //     if (state == States.Init && GUILayout.Button("Start"))
    //     {
    //         fsm.ChangeState(States.Countdown);
    //     }
    //     if (state == States.Countdown)
    //     {
    //         GUILayout.Label("Look at Console");
    //     }
    //     if (state == States.Play)
    //     {
    //         if (GUILayout.Button("Force Win"))
    //         {
    //             fsm.ChangeState(States.Win);
    //         }

    //         GUILayout.Label("Health: " + Mathf.Round(players[0].health).ToString());
    //     }
    //     if (state == States.Win || state == States.Lose)
    //     {
    //         if (GUILayout.Button("Play Again"))
    //         {
    //             fsm.ChangeState(States.Countdown);
    //         }
    //     }

    //     GUILayout.EndArea();
    // }
}
