using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public class Enemy : MonoBehaviour
{
    public GameObject deathSplash;
    public enum States
    {
        Attack,
        Attacked,
        Run,
        Walk,
        Idle,
        Dead
    }
    public BoxCollider mainCollider;
    public CapsuleCollider[] allColliders;
    public float health = 80f;
    public float defense = 10f;
    public float attack = 50f;
    public float attackVelocity = 2f;
    public bool isDead = false;
    public Player target;
    public GameObject hitbox;

    public bool seeking = false;

    private StateMachine<States> fsm;
    private NavMeshAgent agent;
    private Animator anim;
    private GameManager gameManager;

    Rigidbody rb;

    void Awake()
    {
        fsm = StateMachine<States>.Initialize(this, States.Idle);
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        mainCollider = GetComponent<BoxCollider>();
        allColliders = GetComponents<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        gameManager = GameObject.Find("GameController").GetComponent<GameManager>();
    }

    void Update()
    {
        if (isDead) return;
        if ((fsm.State != States.Attacked) && (target == null || target != null && target.isDead)) {
            searchTarget();
        }
        if (seeking == true && fsm.State != States.Attacked && target && !target.isDead)
        {
            agent.SetDestination(target.transform.position);
        }
        if ((fsm.State != States.Walk) && agent.velocity.sqrMagnitude > 0.1f && agent.velocity.sqrMagnitude < 60f)
        {
             fsm.ChangeState(States.Walk);
        }
        if ((fsm.State != States.Attack || fsm.State != States.Attacked) && agent.velocity.sqrMagnitude < 0.1f)
        {
             fsm.ChangeState(States.Idle);
        }
        if ((fsm.State != States.Run) && agent.velocity.sqrMagnitude > 60f)
        {
             fsm.ChangeState(States.Run);
        }
        if (health<1)
        {
            fsm.ChangeState(States.Dead);
        }
    }

    void searchTarget() {
        seeking = true;
        if (gameManager && gameManager.players.Count > 0) {
            target=null;
            foreach (Player player in gameManager.players) {
                if (!player.isDead) {
                    target = player;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!isDead && other.gameObject.CompareTag("Player"))
        {
            Player otherPlayer =  other.gameObject.GetComponent<Player>();
            seeking = false;
            if (!otherPlayer.isDead)
                fsm.ChangeState(States.Attack);
        }
        if (!isDead && other.gameObject.CompareTag("Hitbox_Player"))
        {
            Player otherPlayer =  other.gameObject.transform.parent.GetComponent<Player>();
            DoDamage(otherPlayer.attack);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!isDead && other.gameObject.CompareTag("Player"))
        {
            Player otherPlayer = other.gameObject.GetComponent<Player>();
            seeking=false;
            if (otherPlayer && !otherPlayer.isDead)
                fsm.ChangeState(States.Attack);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!isDead && other.gameObject.CompareTag("Hitbox_Player"))
            fsm.ChangeState(States.Idle);
    }

    public void DoDamage(float damage)
    {
        fsm.ChangeState(States.Attacked);
        float totalDamage = damage * (100 / (100 + defense));
        health -= totalDamage;
    }

    void pushBack() {
        rb.isKinematic = false;
        agent.enabled = false;
        rb.AddForce(-transform.forward * 10, ForceMode.Impulse);
        if(rb.velocity.magnitude <= 0.1f)
        {
            rb.isKinematic = true;
            agent.enabled = true;
        }
    }

    public void DoRagdoll()
    {
        hitbox.SetActive(false);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        foreach (var col in allColliders)
            col.enabled = true;

        mainCollider.enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Animator>().enabled = false;
        GetComponent<LocomotionSimpleAgent>().enabled = false;
        agent.isStopped = true;
        agent.enabled = false;
    }

    public void dropItem()
    {
        int randomNumber = Random.Range(0, gameManager.itemPrefabs.Length);
        Vector3 dropPosition = new Vector3(gameObject.transform.position.x+1.0f, 1.5f, gameObject.transform.position.z+1.0f);
        Instantiate(gameManager.itemPrefabs[randomNumber], dropPosition, Quaternion.identity);
    }

    private void Run_Enter()
    {
        anim.Play("run");
    }

    private void Walk_Enter()
    {
        anim.Play("walk");
    }

    private void Idle_Enter()
    {
        if (anim)
            anim.Play("idle");
    }

    private IEnumerator Attack_Enter()
    {
        anim.Play("attack");
        hitbox.SetActive(true);
        yield return new WaitForSeconds(attackVelocity);
        fsm.ChangeState(States.Idle);
    }

    private void Attack_Exit()
    {
        hitbox.SetActive(false);
    }

    private void Attacked_Enter()
    {
        anim.Play("impact");
        pushBack();
    }

    private void Dead_Enter()
    {
        DoRagdoll();
        isDead=true;
        dropItem();
    }
}