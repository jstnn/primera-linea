using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateItem : MonoBehaviour
{
    GameObject itemToRotate;
    float rotationTime = 180f;
    // Start is called before the first frame update
    void Start()
    {
        itemToRotate = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        itemToRotate.transform.Rotate(Vector3.down * (rotationTime * Time.deltaTime));
    }
}
