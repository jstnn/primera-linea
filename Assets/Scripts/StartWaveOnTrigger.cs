using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWaveOnTrigger : MonoBehaviour
{
    SemihOrhan.WaveOne.WaveManager waveManager;
    SemihOrhan.WaveOne.Demo.DisplayStats displayStats;
    bool waveStarted = false;

    void Start() {
        waveManager = GameObject.Find("Wave Manager").GetComponent<SemihOrhan.WaveOne.WaveManager>();
        displayStats = GameObject.Find("Canvas").GetComponent<SemihOrhan.WaveOne.Demo.DisplayStats>();
    }
    IEnumerator OnTriggerEnter(Collider other)
    {
        if (!waveStarted && other.gameObject.CompareTag("Player"))
        {
            waveManager.StartAllConfigWaves();
            waveStarted = true;
            displayStats.SetCenterText("Preparate se viene una redada");
            yield return new WaitForSeconds(2.0f);
            displayStats.SetCenterText(" ");
        }
    }



}
