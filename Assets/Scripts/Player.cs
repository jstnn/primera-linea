﻿using System;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(QuantumInventory))]
public class Player : MonoBehaviour
{
    public enum States
    {
        Attack,
        Attacked,
        Run,
        Walk,
        Idle,
        Dead
    }
    public List<CapsuleCollider> allColliders;
    public BoxCollider mainCollider;
    public float health = 100f;
    public float defense = 50f;
    public float attack = 50f;
    public bool isDead = false;
    public GameObject hitbox;
    public StateMachine<States> fsm;

    private NavMeshAgent agent;
    private Animator anim;
    private RaycastHit hitInfo = new RaycastHit();
    private QuantumInventory inventory;

    void Awake()
    {
        fsm = StateMachine<States>.Initialize(this, States.Idle);
        anim = GetComponent<Animator>();
        allColliders = new List<CapsuleCollider>(GetComponents<CapsuleCollider>());
        mainCollider = GetComponent<BoxCollider>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Start() {
        EventsManager.SetCameraTarget(gameObject);
    }

    void Update()
    {
        if (isDead || fsm.State == States.Dead || agent.isActiveAndEnabled == false) return;
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
            {
                agent.destination = hitInfo.point;
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            fsm.ChangeState(States.Attack);
        }
        if ((fsm.State != States.Walk) && agent.velocity.sqrMagnitude > 0.1f && agent.velocity.sqrMagnitude < 60f)
        {
             fsm.ChangeState(States.Walk);
        }
        if ((fsm.State != States.Attack || fsm.State != States.Attacked) && agent.velocity.sqrMagnitude < 0.1f)
        {
             fsm.ChangeState(States.Idle);
        }
        if ((fsm.State != States.Run) && (agent.velocity.sqrMagnitude > 60f))
        {
             fsm.ChangeState(States.Run);
        }
        if (health<1)
        {
            fsm.ChangeState(States.Dead);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // if (!isDead && other.gameObject.CompareTag("Enemy"))
        // {
        //     Enemy enemy = other.gameObject.GetComponent<Enemy>(); 
        //     if (!enemy.isDead)
        //         fsm.ChangeState(States.Attack);
        // }
        if (!isDead && other.gameObject.CompareTag("Hitbox_Enemy")) {
            Enemy enemy = other.gameObject.transform.parent.GetComponent<Enemy>();
            DoDamage(enemy.attack);
        }
        if (!isDead && other.gameObject.CompareTag("Item"))
        {
            QuantumItem item = other.GetComponent<QuantumItem>();
            if (item.type != null) {
                EventsManager.AddToInventory(item);
                Destroy(other);
            }
        }
    }

    // void OnTriggerStay(Collider other)
    // {
    //     if (!isDead && other.gameObject.CompareTag("Enemy"))
    //     {
    //         Enemy enemy = other.gameObject.GetComponent<Enemy>(); 
    //         if (!enemy.isDead)
    //             fsm.ChangeState(States.Attack);
    //     }
    // }

    void OnTriggerExit(Collider other)
    {
        if (!isDead && other.gameObject.CompareTag("Hitbox_Enemy"))
            fsm.ChangeState(States.Idle);
    }

    public void AddHealth(float bonus) {
        health = health + bonus;
    }

    public void AddDefense(float bonus) {
        defense = defense + bonus;
    }

    public void DoDamage(float damage)
    {
        EventsManager.ShakeCamera(1.0f);
        fsm.ChangeState(States.Attacked);
        float totalDamage = damage * (100 / (100 + defense));
        health -= totalDamage;
    }

    public void DoRagdoll()
    {
        hitbox.SetActive(false);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        foreach (var col in allColliders)
            col.enabled = true;

        mainCollider.enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Animator>().enabled = false;
        GetComponent<LocomotionSimpleAgent>().enabled = false;
        agent.isStopped = true;
        agent.enabled = false;
    }

    private void Run_Enter()
    {
        anim.Play("run");
    }

    private void Walk_Enter()
    {
        anim.Play("walk");
    }

    private void Idle_Enter()
    {
        if (anim) {
            anim.Play("idle");
        }
    }

    private IEnumerator Attack_Enter() {
        anim.Play("attack");
        hitbox.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        fsm.ChangeState(States.Idle);
    }

    private void Attack_Exit()
    {
        hitbox.SetActive(false);
    }

    private void Attacked_Enter() {
        anim.Play("impact");
        EventsManager.ShakeView();
        // EventsManager.ShakeCamera(1.0f);
    }

    private void Dead_Enter()
    {
        DoRagdoll();
        isDead=true;
        EventsManager.RemovePlayer(gameObject);
        Debug.Log("DIED XXXX LOL");
    }
}
