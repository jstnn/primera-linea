using UnityEngine;
using UnityEngine.EventSystems;
 
public class CloseOnContextLoss : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool inContext;
    GameObject myGO;
    QuantumInventory inventory;
 
    private void Awake()
    {
        myGO = gameObject;
        inventory = GameObject.Find("GameController").GetComponent<GameManager>().players[0].gameObject.GetComponent<QuantumInventory>();
    }
 
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !inContext)
        {
            inventory.Freeze();
            inventory.ActionInventory();
        }
    }
 
    public void OnPointerEnter(PointerEventData eventData)
    {
        inContext = true;
    }
 
    public void OnPointerExit(PointerEventData eventData)
    {
        inContext = false;
    }
}